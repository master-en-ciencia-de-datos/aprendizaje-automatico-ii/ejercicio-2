import matplotlib.pyplot as plt
from clasificacion import Clasificacion
from generar_datos import lista_de_listas_a_lista_y_error
from tqdm import tqdm
import numpy as np

clasificacion = Clasificacion()

clasificadores = ["kmeans", "gmm", "agg"]
list_clusters = list(range(2, 11))
N_ITERACIONES = 50
LIST_OF_N = list(range(N_ITERACIONES))

fig, axs = plt.subplots(3, 4)

for i, clasificador in enumerate(clasificadores):
    list_ch = []
    list_silhouette = []
    list_davis = []
    list_completeness = []
    for iteracion in tqdm(range(N_ITERACIONES)):
        ch = []
        silhouette = []
        davis = []
        completeness = []
        for n_clusters in list_clusters:
            clasificacion.genera_puntos(90)
            if clasificador == "kmeans":
                labels = clasificacion.k_means_classification(n_clusters)
            elif clasificador == "gmm":
                labels = clasificacion.gmm_classification(n_clusters)
            elif clasificador == "agg":
                labels = clasificacion.agg_classification(n_clusters)
            else:
                raise ValueError('Clasificador desconocido')
            _ch, _silhouette, _davis, _completeness = clasificacion.metrics(labels)
            ch.append(_ch)
            silhouette.append(_silhouette)
            davis.append(_davis)
            completeness.append(_completeness)

        list_ch.append(ch)
        list_silhouette.append(silhouette)
        list_davis.append(davis)
        list_completeness.append(completeness)

    ch_mean, ch_std = lista_de_listas_a_lista_y_error(list_ch)
    silhouette_mean, silhouette_std = lista_de_listas_a_lista_y_error(
        list_silhouette)
    davis_mean, davis_std = lista_de_listas_a_lista_y_error(list_davis)
    completeness_mean, completeness_std = lista_de_listas_a_lista_y_error(list_completeness)

    print(f" |  |  |  | {clasificador} |  |  |  | ")
    print(" | ".join([str(cluster) for cluster in list_clusters]))
    print(" | ".join(["{:.2f}".format(x) for x in ch_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in silhouette_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in davis_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in completeness_mean]))
    # print(ch_mean)
    # print(silhouette_mean)
    # print(davis_mean)
    # print(completeness_mean)

