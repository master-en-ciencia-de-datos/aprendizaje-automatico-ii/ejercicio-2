"""
Apartado 2: clasificación no supervisada utilizando:
    - K-medias
    - mezcla de Gaussianas
    - clústering jerárquico (aglomerativo o divisivo)
"""

from generar_datos import GrupoRandom, lista_de_listas_a_lista_y_error
from utils import dunn_fast
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
from sklearn.metrics import calinski_harabasz_score, silhouette_score
from sklearn.metrics import davies_bouldin_score, completeness_score
from tqdm import tqdm
import numpy as np
pi = np.pi


class Clasificacion:
    def __init__(self):
        self.grupo1 = None
        self.grupo2 = None
        self.grupo3 = None

        self.X = None
        self.y = None

    def genera_puntos(self, n_points):
        """
        Genera tres grupos de puntos según los datos del enunciado de la
        práctica. Devuelve las coordenadas como:
        :return: np.array([[x1, y1, z1], [x2, y2, z2], ..., [x90, y90, z90]]),
                 np.array([0, 0, ..., 2]])
        """
        self.grupo1 = GrupoRandom(-pi/8, pi/8, 0, 2*pi, n_points)
        self.grupo2 = GrupoRandom(pi/2-pi/4, pi/2+pi/4, -pi/4, pi/4, n_points)
        self.grupo3 = GrupoRandom(pi/2-pi/4, pi/2+pi/4, pi/2-pi/4, pi/2+pi/4,
                                  n_points)

        coords, labels = [], []

        for i, grupo in enumerate([self.grupo1, self.grupo2, self.grupo3]):
            for x, y, z in zip(grupo.x, grupo.y, grupo.z):
                coords.append(np.array([x, y, z]))
                labels.append(i)

        self.X, self.y = coords, labels

    def plot_generated_data_3d(self, colors=None):
        if colors is None:
            colors = ["black", "black", "black"]
        if self.X is None or self.y is None:
            print("Debes generar primero los puntos.")
            return
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        markers = ["o", "^", "+"]
        for coords, label in zip(self.X, self.y):
            marker = markers[label]
            color = colors[label]
            ax.scatter(coords[0], coords[1], coords[2], marker=marker,
                       c=color)
        ax.set_xlabel('Eje X')
        ax.set_ylabel('Eje Y')
        ax.set_zlabel('Eje Z')

    def plot_generated_data_2d(self, colors=None):
        if colors is None:
            colors = ["black", "black", "black"]
        if self.X is None or self.y is None:
            print("Debes generar primero los puntos.")
            return

        fig2, (axy, axz, ayz) = plt.subplots(1, 3, figsize=(12, 6))
        markers = ["o", "^", "+"]
        for coords, label in zip(self.X, self.y):
            marker = markers[label]
            color = colors[label]
            axy.scatter(coords[0], coords[1], marker=marker, c=color)
            axz.scatter(coords[0], coords[2], marker=marker, c=color)
            ayz.scatter(coords[1], coords[2], marker=marker, c=color)

        fig2.suptitle("Proyecciones de los datos generados")
        axy.set_xlabel("Eje X")
        axy.set_ylabel("Eje Y")
        axz.set_xlabel("Eje X")
        axz.set_ylabel("Eje Z")
        ayz.set_xlabel("Eje Y")
        ayz.set_ylabel("Eje Z")

        axy.set_title("Plano 0XY")
        axz.set_title("Plano 0XZ")
        ayz.set_title("Plano 0YZ")
        fig2.tight_layout()
        fig2.subplots_adjust(top=0.85)
        fig2.savefig("datos_generados.png")

    def k_means_classification(self, n_clusters=3):
        """
        Calcula el clustering para los tres grupos usando K-means.
        :return: las predicciones sobre X
        """
        if self.X is None or self.y is None:
            print("Debes generar primero los puntos.")
            return
        kmeans = KMeans(n_clusters, max_iter=1000).fit(self.X)
        return kmeans.labels_

    def gmm_classification(self, n_clusters=3):
        """
        Calcula el clustering para los tres grupos usando Gaussian Mixture.
        :return: las predicciones sobre X
        """
        if self.X is None or self.y is None:
            print("Debes generar primero los puntos.")
            return
        gmm = GaussianMixture(n_components=n_clusters).fit(self.X)
        predictions = gmm.predict(self.X)
        return predictions

    def agg_classification(self, n_clusters=3):
        """
        Calcula el clustering para los tres grupos usando Agglomerative
        Clustering.
        :return: las predicciones sobre X
        """
        if self.X is None or self.y is None:
            print("Debes generar primero los puntos.")
            return
        agg = AgglomerativeClustering(n_clusters).fit(self.X)
        return agg.labels_

    def plot_clasification(self, prediction, plot=None, title=None):
        markers = ["o", "^", "+"]
        colors = ["r", "g", "b"]
        if plot == "2d":
            fig2, (axy, axz, ayz) = plt.subplots(1, 3)
            fig2.suptitle(title)
            for coords, label, predict in zip(self.X, self.y, prediction):
                marker = markers[label]
                color = colors[predict]
                axy.scatter(coords[0], coords[1], marker=marker, c=color)
                axz.scatter(coords[0], coords[2], marker=marker, c=color)
                ayz.scatter(coords[1], coords[2], marker=marker, c=color)
        elif plot == "3d":
            fig = plt.figure()
            fig.suptitle(title)
            ax = fig.add_subplot(111, projection='3d')
            for coords, label, predict in zip(self.X, self.y, prediction):
                marker = markers[label]
                color = colors[predict]
                ax.scatter(coords[0], coords[1], coords[2], marker=marker,
                           c=color)
            ax.set_xlabel('Eje X')
            ax.set_ylabel('Eje Y')
            ax.set_zlabel('Eje Z')

    def metrics(self, labels):
        """
        Devuelve el valor de las tres métricas para la predicción labels
        :return:
        """

        CH = calinski_harabasz_score(self.X, labels)
        S = silhouette_score(self.X, labels)
        # D = dunn_fast(self.X, labels)
        D = davies_bouldin_score(self.X, labels)
        C = completeness_score(self.y, labels)
        return CH, S, D, C
        # return CH, S, C

