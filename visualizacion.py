import matplotlib.pyplot as plt
from clasificacion import Clasificacion


clasificacion = Clasificacion()
clasificacion.genera_puntos(30)
# ------------------- Apartado 1 -------------------
clasificacion.plot_generated_data_3d(["r", "g", "b"])
clasificacion.plot_generated_data_2d(["r", "g", "b"])
# ------------------- Apartado 2 -------------------
# kmeans_prediction = clasificacion.k_means_classification()
# clasificacion.plot_clasification(kmeans_prediction, "3d", "kmeans")
# gmm_prediction = clasificacion.gmm_classification()
# clasificacion.plot_clasification(gmm_prediction, "3d", "gmm")
# agg_prediction = clasificacion.agg_classification()
# clasificacion.plot_clasification(agg_prediction, "3d", "agg")


plt.show()
