"""
Apartado 1: Generar tres grupos de datos de la forma:
theta_i = U(alfa, beta)
phi_i = U(alfa', beta')
x_i = sin(theta_i)sin(phi_i) + W_i1
y_i = sin(theta_i)sin(phi_i) + W_i2
z_i = cos(theta_i) + W_i3

Donde los U indican una elección aleatoria entre el primer y segundo argumento
y los W son valores de una distribución normal con desviación estándar 0.6.
Cada grupo tiene 30 puntos.
"""

import numpy as np

pi = np.pi


class GrupoRandom:
    """
    Uso:
        grupo1 = GrupoRandom(-pi/8, pi/8, 0, 2*pi)
    """

    def __init__(self, alpha1, beta1, alpha2, beta2, n_points=30):
        random_thetas = np.random.uniform(alpha1, beta1, n_points)
        random_phis = np.random.uniform(alpha2, beta2, n_points)
        random_w = np.random.normal(0, scale=0.6, size=3*n_points)

        self.x = np.multiply(np.sin(random_thetas), np.cos(random_phis)) \
                 + random_w[0:n_points]
        self.y = np.multiply(np.sin(random_thetas), np.sin(random_phis)) \
                 + random_w[n_points:2*n_points]
        self.z = np.cos(random_thetas) + random_w[2*n_points:3*n_points]


def lista_de_listas_a_lista_y_error(lista):
    """
    Convierte un lista de arrays de numpy en una
    :param lista:
    :return:
    """
    mean = np.mean(lista, axis=0)
    std  = np.std(lista, axis=0)
    return mean, std
