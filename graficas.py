import matplotlib.pyplot as plt
from clasificacion import Clasificacion
from generar_datos import lista_de_listas_a_lista_y_error
from tqdm import tqdm
import numpy as np

clasificacion = Clasificacion()

clasificadores = ["kmeans", "gmm", "agg"]
list_clusters = list(range(2, 11))
N_ITERACIONES = 50
LIST_OF_N = list(range(N_ITERACIONES))

fig, axs = plt.subplots(3, 4)

for i, clasificador in enumerate(clasificadores):
    list_ch = []
    list_silhouette = []
    list_davis = []
    list_completeness = []
    for iteracion in tqdm(range(N_ITERACIONES)):
        ch = []
        silhouette = []
        davis = []
        completeness = []
        for n_clusters in list_clusters:
            clasificacion.genera_puntos(90)
            if clasificador == "kmeans":
                labels = clasificacion.k_means_classification(n_clusters)
            elif clasificador == "gmm":
                labels = clasificacion.gmm_classification(n_clusters)
            elif clasificador == "agg":
                labels = clasificacion.agg_classification(n_clusters)
            else:
                raise ValueError('Clasificador desconocido')
            _ch, _silhouette, _davis, _completeness = clasificacion.metrics(labels)
            ch.append(_ch)
            silhouette.append(_silhouette)
            davis.append(_davis)
            completeness.append(_completeness)

        list_ch.append(ch)
        list_silhouette.append(silhouette)
        list_davis.append(davis)
        list_completeness.append(completeness)

    ch_mean, ch_std = lista_de_listas_a_lista_y_error(list_ch)
    silhouette_mean, silhouette_std = lista_de_listas_a_lista_y_error(
        list_silhouette)
    davis_mean, davis_std = lista_de_listas_a_lista_y_error(list_davis)
    completeness_mean, completeness_std = lista_de_listas_a_lista_y_error(list_completeness)

    # fig.suptitle(clasificador)

    axs[i, 0].plot(list_clusters, ch_mean, label="CH", color="royalblue")
    axs[i, 0].fill_between(list_clusters, ch_mean - ch_std,
                           ch_mean + ch_std,
                           alpha=0.2, edgecolor='lightsteelblue',
                           facecolor='cornflowerblue',
                           linewidth=4, linestyle='dashdot', antialiased=True)
    axs[i, 0].set_title(f"{clasificador}_ch")

    axs[i, 1].plot(list_clusters, silhouette_mean, label="Silhouette", color="royalblue")
    axs[i, 1].fill_between(list_clusters, silhouette_mean - silhouette_std,
                           silhouette_mean + silhouette_std,
                           alpha=0.2, edgecolor='lightsteelblue',
                           facecolor='cornflowerblue',
                           linewidth=4, linestyle='dashdot', antialiased=True)
    axs[i, 1].set_title(f"{clasificador}_silhouette")

    axs[i, 2].plot(list_clusters, davis_mean, label="Davis", color="royalblue")
    axs[i, 2].fill_between(list_clusters, davis_mean - davis_std,
                           davis_mean + davis_std,
                           alpha=0.2, edgecolor='lightsteelblue',
                           facecolor='cornflowerblue',
                           linewidth=4, linestyle='dashdot', antialiased=True)
    axs[i, 2].set_title(f"{clasificador}_davis")

    axs[i, 3].plot(list_clusters, completeness_mean, label="completeness", color="royalblue")
    axs[i, 3].fill_between(list_clusters, completeness_mean - completeness_std,
                           completeness_mean + completeness_std,
                           alpha=0.2, edgecolor='lightsteelblue',
                           facecolor='cornflowerblue',
                           linewidth=4, linestyle='dashdot', antialiased=True)
    axs[i, 3].set_title(f"{clasificador}_completeness")

    axs[i, 0].set_xticks(np.arange(list_clusters[0], list_clusters[-1]+1, 1.0))
    axs[i, 1].set_xticks(np.arange(list_clusters[0], list_clusters[-1]+1, 1.0))
    axs[i, 2].set_xticks(np.arange(list_clusters[0], list_clusters[-1]+1, 1.0))
    axs[i, 3].set_xticks(np.arange(list_clusters[0], list_clusters[-1]+1, 1.0))

    print(f" |  |  |  | {clasificador} |  |  |  | ")
    print(" | ".join([str(cluster) for cluster in list_clusters]))
    print(" | ".join(["{:.2f}".format(x) for x in ch_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in silhouette_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in davis_mean]))
    print(" | ".join(["{:.2f}".format(x) for x in completeness_mean]))
    print("-----------------------------------")

plt.show()